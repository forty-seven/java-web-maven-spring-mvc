<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:url value="/login-post" var="loginUrl" />
<tiles:insertDefinition name="home">
	<tiles:putAttribute name="title" value="Login" />
	<tiles:putAttribute name="body">
		<div class="container">
			<div class="row" style="margin-top: 150px">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Please Sign In</h3>
						</div>
						<div class="panel-body">
							<form name="loginForm" action="${loginUrl}" method='POST'>
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<fieldset>
									<div class="col-md-12" style="color: red; text-align: center; margin-bottom: 10px"></div>
									<div class="form-group">
										<input type="text" name="userName" class="form-control" />
									</div>
									<div class="form-group">
										<input type="password" name="password" class="form-control" />
									</div>
									<div class="checkbox">
										<label> <input type="checkbox" name="remember" value="true" />Remember Me
										</label>
									</div>
									<!-- Change this to a button or input when using this as a form -->
									<input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>

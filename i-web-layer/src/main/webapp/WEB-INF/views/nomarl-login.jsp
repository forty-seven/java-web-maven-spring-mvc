<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<tiles:insertDefinition name="home">
	<tiles:putAttribute name="title" value="Login" />
	<tiles:putAttribute name="body">
		<div class="container">
		<h1>FUCK</h1>
			<div class="row" style="margin-top:150px">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Please Sign In</h3>
						</div>
						<div class="panel-body">
							<form:form action="${pageContext.request.contextPath}/login-post" method="post" autocomplete="off" >
<%-- 								<form:hidden path="${_csrf.parameterName}" value="${_csrf.token}"/>  --%>
								<fieldset>
									<div class="col-md-12" style="color: red; text-align: center; margin-bottom: 10px">
										
									</div>
									<div class="form-group">
										<form:input path="userName" cssClass="form-control" />
										<form:errors path="userName" cssStyle="color:red" ></form:errors>
									</div>
									<div class="form-group">
										<form:password path="password" cssClass="form-control"/>
										<form:errors path="password" cssStyle="color:red" ></form:errors>
									</div>
									<div class="checkbox">
										<label> <form:checkbox path="remember" value="true" />Remember Me</label>
									</div>
									<!-- Change this to a button or input when using this as a form -->
									<input type="submit" class="btn btn-lg btn-success btn-block" value="Login"/>
								</fieldset>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>


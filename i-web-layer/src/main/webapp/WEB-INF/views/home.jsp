<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="initial-scale=1.0, width=device-width" name="viewport">
<title>Vominh</title>
<spring:url value="/resources" var="resourcesPath" />

<jsp:include page="partial/CommonCss.jsp" />
<link href="${resourcesPath}/site.css" rel="stylesheet">

<jsp:include page="partial/CommonJs.jsp" />
<script src="${resourcesPath}/js/_home-animation.js"></script>

</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top my-navbar">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
				aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Project name</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#contact">Contact</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Nav header</li>
						<li><a href="#">Separated link</a></li>
						<li><a href="#">One more separated link</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="../navbar/">Default</a></li>
				<li><a href="../navbar-static-top/">Static top</a></li>
				<li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
			</ul>
		</div>
	</div>
	</nav>

	<section class="home">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-lg-6 my-logo">
				<h1>I LOVE FLOWER</h1>
				<div class="logo-image">
					<img src="${resourcesPath}/_image/mobile-logo-2.png">
				</div>
			</div>
		</div>
	</div>
	</section>

	<section class="solution">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 my-solution">
				<h1>Solution</h1>
			</div>
		</div>

		<div class="row margin-bottom-30 solution-row">

			<div class="col-xs-6 col-sm-4 col-lg-4">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Power Save </span>
						<div class="description display-none">
							<span class="block-text description"> What Samsung's power saving mode can do and how it can help you out
								when you're short on battery</span>
							<button class="btn btn-primary margin-top-5">How it work</button>
							<button class="btn btn-danger margin-top-5">Try this</button>
							<button class="btn btn-success margin-top-5">Price</button>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-6 col-sm-4 col-lg-4">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-grain" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Green Life </span>
						<div class="description display-none">
							<span class="block-text description"> What Samsung's power saving mode can do and how it can help you out
								when you're short on battery</span>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-4 col-lg-4 margin-top-30-mobile">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Smart House </span> <span class="block-text description display-none"> What
							Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
					</div>
				</a>
			</div>
		</div>

		<div class="row solution-row">
			<div class="col-xs-6 col-sm-4 col-lg-4">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Mobile Services </span> <span class="block-text description display-none">
							What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
					</div>
				</a>
			</div>

			<div class="col-xs-6 col-sm-4 col-lg-4">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Global Warming </span> <span class="block-text description display-none">
							What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-4 col-lg-4 margin-top-30-mobile">
				<a href="#">
					<div class="solution-block color-white">
						<div class="block-image">
							<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
						</div>
						<span class="block-text title"> Broken Hearts </span> <span class="block-text description display-none">
							What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
					</div>
				</a>
			</div>
		</div>
	</div>
	</section>


	<section class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1>Vominh's Blog</h1>
			</div>

		</div>

		<div class="row">
			<div class="col-xs-12">
				<h3>Pham Duc Minh</h3>
			</div>
		</div>
	</div>
	</section>


</body>
</html>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<tiles:insertDefinition name="home">
	<tiles:putAttribute name="title" value="HOME HOME HOME"/>
    <tiles:putAttribute name="body">
        <section class="home">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 col-lg-6 my-logo">
						<h1>I LOVE FLOWER</h1>
						<div class="logo-image">
							<img src="${pageContext.request.contextPath}/resources/_image/mobile-logo-2.png">
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="solution">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 my-solution">
						<h1>Solution</h1>
					</div>
				</div>
		
				<div class="row margin-bottom-30 solution-row">
		
					<div class="col-xs-6 col-sm-4 col-lg-4">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Power Save </span>
								<div class="description display-none">
									<span class="block-text description"> What Samsung's power saving mode can do and how it can help you out
										when you're short on battery</span>
									<button class="btn btn-primary margin-top-5">How it work</button>
									<button class="btn btn-danger margin-top-5">Try this</button>
									<button class="btn btn-success margin-top-5">Price</button>
								</div>
							</div>
						</a>
					</div>
		
					<div class="col-xs-6 col-sm-4 col-lg-4">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-grain" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Green Life </span>
								<div class="description display-none">
									<span class="block-text description"> What Samsung's power saving mode can do and how it can help you out
										when you're short on battery</span>
								</div>
							</div>
						</a>
					</div>
		
					<div class="col-xs-12 col-sm-4 col-lg-4 margin-top-30-mobile">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Smart House </span> <span class="block-text description display-none"> What
									Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
							</div>
						</a>
					</div>
				</div>
		
				<div class="row solution-row">
					<div class="col-xs-6 col-sm-4 col-lg-4">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Mobile Services </span> <span class="block-text description display-none">
									What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
							</div>
						</a>
					</div>
		
					<div class="col-xs-6 col-sm-4 col-lg-4">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Global Warming </span> <span class="block-text description display-none">
									What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
							</div>
						</a>
					</div>
		
					<div class="col-xs-12 col-sm-4 col-lg-4 margin-top-30-mobile">
						<a href="#">
							<div class="solution-block color-white">
								<div class="block-image">
									<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
								</div>
								<span class="block-text title"> Broken Hearts </span> <span class="block-text description display-none">
									What Samsung's power saving mode can do and how it can help you out when you're short on battery </span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
    </tiles:putAttribute>
</tiles:insertDefinition>


<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width" />
	<title><tiles:insertAttribute name="title" ignore="true" /></title>
	
	<spring:url value="/resources" var="resourcesPath" />

	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="${resourcesPath}/css/bootstrap.min.css">
	<link rel="stylesheet" href="${resourcesPath}/common.css">
	<link rel="stylesheet" href="${resourcesPath}/site.css" >
	
	<!-- jQuery 1.12.0 -->
	<script src="${resourcesPath}/js/jquery-1.12.0.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="${resourcesPath}/js/bootstrap.min.js"></script>
	
	<script src="${resourcesPath}/js/_home-animation.js"></script>
	

	
</head>
<body>
	<section class="header">
		<tiles:insertAttribute name="header"/>
	</section>
	<section class="body">
		<tiles:insertAttribute name="body" />
	</section>
	<section class="footer">
		<tiles:insertAttribute name="footer" />
	</section>
</body>
</html>
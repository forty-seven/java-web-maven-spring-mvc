package com.vominh.model;

import org.hibernate.validator.constraints.NotEmpty;

public class LoginModel
{
	@NotEmpty(message="User name cannot be empty")
	private String userName;
	
	@NotEmpty(message="Password cannot be empty")
	private String password;
	
	private String message;
	private boolean remember;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public boolean isRemember()
	{
		return remember;
	}

	public void setRemember(boolean remember)
	{
		this.remember = remember;
	}

}


package com.vominh.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vominh.model.LoginModel;

@Controller
public class HomeController
{
	// Spring Security see this
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login()
	{
		ModelAndView mav = new ModelAndView();
		LoginModel loginModel = new LoginModel();
		mav.addObject("command", loginModel);
		mav.setViewName("login");
		return mav;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView loginProcess(@ModelAttribute("command") @Valid LoginModel loginModel, BindingResult result)
	{
		ModelAndView mav = null;

		// Input validate
		if (result.hasErrors())
		{
			System.out.println(result.getErrorCount());
			System.out.println(result.getAllErrors().toString());
			mav = new ModelAndView("login", "command", loginModel);
			return mav;

		}
		else
		{

			// Business validate
			if (loginModel.getUserName() != "docvominh")
			{
				loginModel.setMessage("something Wrong !");
				System.out.println(loginModel.getUserName());
				mav = new ModelAndView("login", "command", loginModel);
				return mav;
			}

			return welcome();
		}

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcome()
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("home2");
		return mav;
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public ModelAndView about()
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("about");
		return mav;
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public ModelAndView contact()
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("contact");
		return mav;
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView admin()
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin");
		return mav;
	}

}

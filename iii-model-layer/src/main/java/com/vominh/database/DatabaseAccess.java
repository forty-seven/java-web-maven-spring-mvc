package com.vominh.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DatabaseAccess {

	private static final Logger logger = LogManager.getLogger(DatabaseAccess.class);
	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	public Object selectOne(String sql, Class<?> tClass, Map<String, Object> queryParam) {
		SqlSession session = null;
		Object returnObject = null;

		// List<Class<T>> list = new ArrayList<Class<T>>();

		try {
			returnObject = tClass.newInstance();
			session = sqlSessionFactory.openSession();
			returnObject = session.selectOne(sql, queryParam);

		} catch (Exception e) {
			logger.error(e);
		} finally {
			session.close();
		}

		return returnObject;

	}

	@SuppressWarnings("rawtypes")
	public List selectList(String sql, Map<String, Object> queryParam) {
		SqlSession session = null;

		List list = new ArrayList();

		try {
			session = sqlSessionFactory.openSession();
			list = session.selectList(sql, queryParam);
		} catch (Exception e) {
			logger.error(e);
		} finally {
			session.close();
		}

		return list;

	}

	@SuppressWarnings("rawtypes")
	public List selectList(String sql, Object queryParam) {
		SqlSession session = null;

		List list = new ArrayList();

		try {
			session = sqlSessionFactory.openSession();
			list = session.selectList(sql, queryParam);
		} catch (Exception e) {
			logger.error(e);
		} finally {
			session.close();
		}

		return list;

	}
}
